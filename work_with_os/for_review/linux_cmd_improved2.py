import subprocess
import datetime
result = subprocess.run(['ps', 'aux'], stdout=subprocess.PIPE)

output = result.stdout.decode('utf-8')
lines = output.splitlines()

users = {}
processes = []

#for line in lines:
for line in lines[1:]:

    user, PID, CPU, MEM, VSZ, RSS, TTY, STAT, START, TIME, *COMMAND  = line.split()
    memory = float(MEM)

    if user in users:
        users[user]['processes'] += 1
        users[user]['memory'] += memory
    else:
        users[user] = {'processes': 1, 'memory': memory}

        processes.append({
        'user': user,
        'cpu': float(CPU),
        'memory': memory,
        'name': COMMAND[0]
        })


user_sys = ', '.join(users.keys())
user_processes = "".join(f"{user}: {data['processes']}\n" for user, data in users.items())

memory_total = sum(data['memory'] for data in users.values())
cpu_total = sum(process['cpu'] for process in processes)
most_memory_process = max(processes, key=lambda process: process['memory'])
most_cpu_process = max(processes, key=lambda process: process['cpu'])

template = (f'Отчёт о состоянии системы:\n'
            f'Пользователи системы: {user_sys}\n' 
            f'Процессов запущено: {len(processes)}\n'
            f'Пользовательских процессов: \n{user_processes}'
            f'Всего памяти используется: {memory_total} mb\n'
            f'Всего CPU используется: {cpu_total}%\n'
            f"Больше всего памяти использует: {most_memory_process['name']} {most_memory_process['memory']} mb\n"
            f"Больше всего CPU использует: ({most_cpu_process['name']}...) {most_cpu_process['cpu']} %\n"
            )

print(template)


filename = datetime.datetime.now().strftime('%d-%m-%Y-%H:%M-scan.txt')
with open(filename, 'w') as f:
    f.write(template)

