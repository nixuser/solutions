import subprocess as sp
import datetime as dt
from collections import defaultdict
from jinja2 import Template

# Constants
REPORT_TEMPLATE = 'report.template'
OUTPUT_FILE_PATTERN = '%d-%m-%Y-%H:%M-scan.txt'

def get_system_info():
    result = sp.run(['ps', 'aux'], stdout=sp.PIPE)
    output = result.stdout.decode('utf-8')
    lines = output.splitlines()
    return lines[1:]  # skip header

def parse_process_line(line):
    user, PID, CPU, MEM, VSZ, RSS, TTY, STAT, START, TIME, *COMMAND = line.split()
    return {
        'user': user,
        'cpu': float(CPU),
        'memory': float(MEM),
        'name': COMMAND[0]
    }

def group_processes_by_user(processes):
    users = defaultdict(lambda: {'processes': 0, 'memory': 0.0})
    for process in processes:
        users[process['user']]['processes'] += 1
        users[process['user']]['memory'] += process['memory']
    return users

def calculate_totals(users, processes):
    memory_total = sum(user['memory'] for user in users.values())
    cpu_total = sum(process['cpu'] for process in processes)
    most_memory_process = max(processes, key=lambda process: process['memory'])
    most_cpu_process = max(processes, key=lambda process: process['cpu'])
    return memory_total, cpu_total, most_memory_process, most_cpu_process

def generate_report(users, memory_total, cpu_total, most_memory_process, most_cpu_process):
    template = Template(open(REPORT_TEMPLATE).read())
    user_sys = ', '.join(users.keys())
    user_processes = '\n'.join(f"{user}: {data['processes']}" for user, data in users.items())
    return template.render(
        user_sys=user_sys,
        user_processes=user_processes,
        memory_total=memory_total,
        cpu_total=cpu_total,
        most_memory_process=f"{most_memory_process['name']} {most_memory_process['memory']}",
        most_cpu_process=f"({most_cpu_process['name']}...) {most_cpu_process['cpu']}"
    )

def main():
    lines = get_system_info()
    processes = [parse_process_line(line) for line in lines]
    users = group_processes_by_user(processes)
    memory_total, cpu_total, most_memory_process, most_cpu_process = calculate_totals(users, processes)
    report = generate_report(users, memory_total, cpu_total, most_memory_process, most_cpu_process)
    filename = dt.datetime.now().strftime(OUTPUT_FILE_PATTERN)
    with open(filename, 'w') as f:
        f.write(report)
    print(report)

if __name__ == '__main__':
    main()
