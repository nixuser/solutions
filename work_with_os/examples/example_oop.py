import subprocess
from collections import defaultdict
from datetime import datetime

class SystemStatus:
    def __init__(self):
        self.users = []
        self.processes = defaultdict(list)
        self.process_count = 0
        self.total_memory = 0.0
        self.total_cpu = 0.0
        self.most_mem_process = None
        self.most_cpu_process = None

    def get_system_status(self):
        ps_aux_output = subprocess.check_output(['ps', 'aux']).decode().split('\n')
        for line in ps_aux_output[1:]:
            if not line:
                continue
            parts = line.split()
            self.users.append(parts[0])
            self.processes[parts[0]].append(parts)

        # Removing duplicates
        self.users = list(set(self.users))
        
        # Total process count
        self.process_count = sum(len(value) for value in self.processes.values())
        
        # Total memory and cpu usage
        for procs in self.processes.values():
            for parts in procs:
                self.total_memory += float(parts[3])
                self.total_cpu += float(parts[2])

        # Process using most memory and cpu
        self.most_mem_process = max((max(procs, key=lambda p: float(p[3])) for procs in self.processes.values()), key=lambda p: float(p[3]))
        self.most_cpu_process = max((max(procs, key=lambda p: float(p[2])) for procs in self.processes.values()), key=lambda p: float(p[2]))

    def print_report(self):
        report = '\n'.join([
            "System Status Report:",
            "System users: " + ', '.join(self.users),
            "Processes running: " + str(self.process_count),
            "",
            "User processes:",
            '\n'.join([f"{user}: {len(procs)}" for user, procs in self.processes.items()]),
            "",
            "Total memory used: " + str(self.total_memory) + " mb",
            "Total CPU used: " + str(self.total_cpu) + "%",
            "Uses the most memory: " + self.most_mem_process[10][:20],
            "Uses the most CPU: " + self.most_cpu_process[10][:20],
        ])
 
        print(report)

        filename = datetime.now().strftime('%d-%m-%Y-%H-%M-scan.txt')
        with open(filename, 'w') as f:
            f.write(report)


if __name__ == "__main__":
    sys_status = SystemStatus()
    sys_status.get_system_status()
    sys_status.print_report()
