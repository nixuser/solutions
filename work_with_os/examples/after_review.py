import subprocess
from datetime import datetime

# Constants
MEM_CONVERSION_FACTOR = 1024


def ps_aux():
    stdout = subprocess.run(["ps", "aux"], capture_output=True).stdout.decode().splitlines()
    stdout_headers = stdout[0].split()
    stdout_data = [stdout_row.split(None, len(stdout_headers) - 1) for stdout_row in stdout[1:]]
    return [dict(zip(stdout_headers, stdout_row)) for stdout_row in stdout_data]


def get_users(process_result: list):
    return [row["USER"] for row in process_result]


def get_user_process(users: list) -> list:
    user_process = []
    for user in set(users):
        user_process.append(f"{user}:{users.count(user)}")
    return user_process


def get_resource_usage(process_result: list):
    cpu_usage = 0
    mem_usage = 0

    for row in process_result:
        cpu_usage += float(row["%CPU"])
        mem_usage += int(row["RSS"])

    return round(cpu_usage, 1), round(mem_usage / MEM_CONVERSION_FACTOR, 1)


def get_max_process(process_result: list, value: str) -> str:
    max_process = max(process_result, key=lambda x: float(x[value]))
    return max_process["COMMAND"]

  
process_report = ps_aux()
users = get_users(process_report)
max_rss_process = get_max_process(process_report, 'RSS')
max_cpu_process = get_max_process(process_report, '%CPU')
cpu_usage, mem_usage = get_resource_usage(process_report)
user_procs = get_user_process(users)
date_str = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

data = [
    f"Отчёт о состоянии системы: ",
    f"Пользователи системы: {set(users)} ",
    f"Процессов запущено: {len(process_report)}",
    f"Пользовательских процессов: {user_procs}",
    f"Всего памяти используется: {mem_usage} mb",
    f"Всего CPU используется: {cpu_usage}%",
    f"Больше всего памяти использует: {max_rss_process[:20]}",
    f"Больше всего CPU использует: {max_cpu_process[:20]}"
]

with open(f'{date_str}-report.txt', 'w') as f:
    for row in data:
        f.write(f"{row}\n")
