import subprocess
from collections import Counter, defaultdict
from datetime import datetime

def parse_ps_aux():
    # Get the output of the 'ps aux' command
    ps_aux_output = subprocess.check_output(['ps', 'aux']).decode().split('\n')

    user_procs = defaultdict(list)
    
    # Skip the first line (column headers)
    for line in ps_aux_output[1:]:
        if not line:
            continue
        parts = line.split()
        user_procs[parts[0]].append(parts)

    users = list(user_procs.keys())
    num_procs = sum(len(value) for value in user_procs.values())

    user_num_procs = {user: len(procs) for user, procs in user_procs.items()}

    total_memory = sum(float(parts[3]) for parts in user_procs.values())
    total_cpu = sum(float(parts[2]) for parts in user_procs.values())

    most_memory_proc = max((max(procs, key=lambda p: float(p[3])) for procs in user_procs.values()), key=lambda p: float(p[3]))
    most_cpu_proc = max((max(procs, key=lambda p: float(p[2])) for procs in user_procs.values()), key=lambda p: float(p[2]))

    
    # Create a readable report
    report = '\n'.join([
        "System Status Report:",
        "System users: " + ', '.join(users),
        "Processes running: " + str(num_procs),
        "",
        "User processes:",
        '\n'.join([f"{user}: {num_procs}" for user, num_procs in user_num_procs.items()]),
        "",
        "Total memory used: " + str(total_memory) + " mb",
        "Total CPU used: " + str(total_cpu) + "%",
        "Uses the most memory: " + most_memory_proc[10][:20],
        "Uses the most CPU: " + most_cpu_proc[10][:20],
    ])

    # Output the report to standard out
    print(report)

    # Save the report to a file
    filename = datetime.now().strftime('%d-%m-%Y-%H-%M-scan.txt')
    with open(filename, 'w') as f:
        f.write(report)


if __name__ == "__main__":
    parse_ps_aux()

