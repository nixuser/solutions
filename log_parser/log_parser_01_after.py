import json
import os
import re
from collections import defaultdict
from pprint import pprint

import click


class ParseLogsException(BaseException):
    def __init__(self, path: str):
        self.path = path

    def __str__(self):
        return f'ParseLogsException Error: Check the path to log file or directory {self.path} or check your credentials.'


METHOD_PATTERN = re.compile(r'(POST|GET|PUT|DELETE|HEAD|OPTIONS)\b')
REMOTE_HOST_PATTERN = re.compile(r'^(?:\d{1,3}\.){3}\d{1,3}')
DURATION_PATTERN = re.compile(r'\s\d+\n')
DATE_PATTERN = re.compile(r'\[[\w\s/:+-]*\]')
URL_PATTERN = re.compile(r'\"https?://(\S*)\"')


def parse_logs(log_file):
    with open(log_file, 'r') as file:
        requests_number = 0
        methods_dict = defaultdict(int)
        all_requests = []
        dict_ip_requests = defaultdict(lambda: {"requests_number": 0})

        for line in file:
            method = METHOD_PATTERN.search(line).group(0)
            remote_host = REMOTE_HOST_PATTERN.search(line).group(0)
            request_duration = int(DURATION_PATTERN.search(line).group(0).replace('\n', ''))
            date = DATE_PATTERN.search(line).group(0)
            is_url = URL_PATTERN.search(line)
            url = is_url.group(0)[1:-1] if is_url else '-'

            request_info = {
                'method': method,
                'ip': remote_host,
                'duration': request_duration,
                'date': date,
                'url': url,
            }
            all_requests.append(request_info)

            requests_number += 1
            methods_dict[method] += 1
            dict_ip_requests[request_info['ip']]["requests_number"] += 1

        top_ips = dict(sorted(dict_ip_requests.items(), key=lambda x: x[1]["requests_number"], reverse=True)[0:3])
        top_durations = sorted(all_requests, key=lambda x: x['duration'], reverse=True)[0:3]

        result = {
            'total_requests': requests_number,
            'total_methods': dict(methods_dict),
            'top_ips': {k: v['requests_number'] for k, v in top_ips.items()},
            'top_longest_requests': top_durations}

        with open(f'{log_file}.json', 'w') as json_file:
            json.dump(result, json_file, indent=4)

        pprint(result)


@click.command()
@click.option('--log_file', help='Path to log file or log directory.')
def main(log_file):
    if os.path.exists(log_file):
        if os.path.isfile(log_file):
            parse_logs(log_file)
        elif os.path.isdir(log_file):
            for file in os.listdir(log_file):
                if file.endswith(".log"):
                    path_to_logfile = os.path.join(log_file, file)
                    parse_logs(path_to_logfile)
    else:
        raise ParseLogsException(log_file)


if __name__ == '__main__':
    main()
