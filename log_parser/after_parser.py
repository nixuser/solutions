import argparse
import json
import os
import re
from collections import defaultdict

class LogParser:
    IP_REGEX = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
    METHOD_REGEX = re.compile(r"(POST|GET|PUT|DELETE|HEAD|OPTIONS)")

    def __init__(self, file_path):
        self.file_path = file_path
        self.total_requests = 0
        self.methods = defaultdict(int)
        self.ips = defaultdict(int)
        self.longest_requests = []

    def process_log_file(self):
        with open(self.file_path, 'r') as file:
            for line in file:
                self.total_requests += 1
                ip = self.IP_REGEX.findall(line)[0]
                self.ips[ip] += 1
                try:
                    method = self.METHOD_REGEX.findall(line)[0]
                    self.methods[method] += 1
                except IndexError:
                    continue
                
                tokens = re.split(r'\s+', line)
                if tokens[-1].isdigit():
                    request_duration = int(tokens[-1])
                    if len(self.longest_requests) < 3 or request_duration > min(self.longest_requests, key = lambda x: x['duration'])['duration']:
                        self.longest_requests.append(
                            {'duration': request_duration, 'ip': ip, 'method': method, 'timestamp': tokens[1]})
                        if len(self.longest_requests) > 3:
                            self.longest_requests.remove(min(self.longest_requests, key = lambda x: x['duration']))

    def output(self):
        with open(f"{self.file_path}.json", 'w') as file:
            result = json.dumps({
                'total_requests': self.total_requests,
                'method_counts': dict(self.methods),
                'top_ips': dict(sorted(self.ips.items(), key=lambda item: item[1], reverse=True)[:3]),
                'longest_requests': sorted(self.longest_requests, key = lambda x: x['duration'], reverse=True)},
                indent=4)
            file.write(result)
            print(f"\n===== LOG FILE: {self.file_path} =====\n {result}")

def process_logs(path):
    if os.path.exists(path):
        if os.path.isfile(path):
            parser = LogParser(path)
            parser.process_log_file()
            parser.output()
        elif os.path.isdir(path):
            for subdir, dirs, files in os.walk(path):
                for file in files:
                    if file.endswith(".log"):
                        file_path = os.path.join(subdir, file)
                        parser = LogParser(file_path)
                        parser.process_log_file()
                        parser.output()
    else:
        print("ERROR: Provided path does not exist")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process access logs")
    parser.add_argument("-l", dest="log", action="store", help="Path to logfile or logdir")
    args = parser.parse_args()
    process_logs(args.log)
