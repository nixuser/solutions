import os
import json
import argparse
from collections import Counter, defaultdict
import re

class LogStats:
    def __init__(self, file_path):
        self.file_path = file_path
        self.total_requests = 0
        self.method_count = defaultdict(int)
        self.ip_count = defaultdict(int)
        self.longest_requests = []

    def process_log(self):
        with open(self.file_path, 'r') as f:
            for line in f.readlines():
                self.total_requests += 1
                tokens = re.split(r'\s+', line)
                
                method_url_version = tokens[2]
                method = method_url_version.split(" ")[0]
                self.method_count[method] += 1

                ip = tokens[0]
                self.ip_count[ip] += 1
                
                request_time = int(tokens[-1])
                method_url = " ".join(method_url_version.split(" ")[:2])
                if len(self.longest_requests) < 3 or request_time > self.longest_requests[0][0]:
                    self.longest_requests.append((request_time, ip, method_url, tokens[1]))
                    self.longest_requests.sort()
                    if len(self.longest_requests) > 3:
                        self.longest_requests.pop(0)

    def print_stats(self):
        print(f"Total requests: {self.total_requests}")
        print(f"Method counts: {dict(self.method_count)}")
        print(f"Top 3 IPs: {dict(Counter(self.ip_count).most_common(3))}")
        print(f"Top 3 longest requests: {self.longest_requests}")
        print("\n")

    def save_stats(self):
        with open('log_stats.json', 'w') as f:
            json.dump({
                'total_requests': self.total_requests,
                'method_counts': dict(self.method_count),
                'top_ips': dict(Counter(self.ip_count).most_common(3)),
                'longest_requests': self.longest_requests
            }, f)


def analyze_logs(path):
    if os.path.isfile(path):
        log_stats = LogStats(path)
        log_stats.process_log()
        log_stats.print_stats()
        log_stats.save_stats()
    elif os.path.isdir(path):
        for subdir, dirs, files in os.walk(path):
            for file in files:
                if file.endswith('access.log'):
                    file_path = os.path.join(subdir, file)
                    log_stats = LogStats(file_path)
                    log_stats.process_log()
                    log_stats.print_stats()
                    log_stats.save_stats()

    else:
        print("Please provide a valid directory or log file")
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyze Apache access logs')
    parser.add_argument('path', help='Path to log file or directory containing log files')
    args = parser.parse_args()

    analyze_logs(args.path)
